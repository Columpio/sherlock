module SherlockParser where

import qualified Data.Char as Char
import qualified Data.List as List
import qualified Data.Set  as Set
import qualified Data.Text.Lazy as Text


removePuncts :: Text.Text -> Text.Text
removePuncts = Text.map (\c -> if c == '\'' || Char.isLetter c then c else ' ')

groupLen :: [String] -> [(Int, Set.Set String)]
groupLen text =
    map (\seq -> (length $ head seq, Set.fromList seq)) $   -- (len words, words width such length)
    List.groupBy (\x y -> length x == length y) text        -- [[words with same length]]


voc2Map :: Text.Text -> [(Int, Set.Set String)]
voc2Map text = groupLen $   -- Group words
    map Text.unpack $       -- Each sentence -> [Word]
    Text.lines text         -- Text -> Sentences


enc2Set :: Text.Text -> Set.Set String
enc2Set text = Set.fromList $               -- remove duplicates
    filter (notElem '\'') $                 -- remove quoted words
    map Text.unpack $                       -- Type coerce
    concatMap (Text.words . removePuncts)   -- [Sentences] -> [Words]
    (Text.lines text)                       -- Text -> Sentences

enc2Map :: Text.Text -> [(Int, Set.Set String)]
enc2Map text = groupLen $ reverse $ List.sortOn length $ Set.toList $ enc2Set text


intersect :: [(Int, Set.Set String)] -> [(Int, Set.Set String)] -> [(Int, Set.Set String)]
intersect _ [] = []
intersect [] _ = []
intersect mx@(xh@(xi,_):xs) my@((yi,_):ys)
    | xi < yi = intersect mx ys
    | xi > yi = intersect xs my
    | otherwise = xh : intersect xs ys
